FROM python:3.9

WORKDIR /app

# Copy the necessary files
COPY alphabet_soup4.py .
COPY test_word_search.py .
COPY manual_run.py .
COPY run_alphabetsoup.sh .
COPY input.txt .
COPY input2.txt .
COPY input3.txt .

# Install dependencies
RUN pip install mock

# Set the entry point
ENTRYPOINT ["/app/run_alphabetsoup.sh"]