#!/bin/bash

# Prompt the user to choose an option
echo "Select an option:"
echo "1. Enter filename"
echo "2. Run mock test"
read -p "Option: " option

# Handle the selected option
if [[ $option == "1" ]]; then
    read -p "Enter the filename: " filename
    python manual_run.py "$filename"
elif [[ $option == "2" ]]; then
    python test_word_search.py
else
    echo "Invalid option selected."
fi