import unittest
from unittest.mock import patch
from alphabet_soup4 import WordSearch

class WordSearchTestCase(unittest.TestCase):
    """
    Test case for the WordSearch class.

    Methods:
    - test_search_word(self): Tests the search_word method of the WordSearch class.
    """
    def test_search_word(self):
        """
        Tests the search_word method of the WordSearch class.

        This method verifies that the search_word method returns the correct positions of a word in the puzzle grid.

        Returns:
        - None
        """
        grid = [
            ['H', 'E', 'L', 'L', 'O'],
            ['O', 'A', 'O', 'O', 'D'],
            ['B', 'Y', 'I', 'L', 'L'],
            ['W', 'E', 'R', 'L', 'D'],
            ['W', 'O', 'R', 'L', 'D']
        ]

        word_search = WordSearch(grid=grid)

        # Test horizontal search
        result = word_search.search_word('HELLO')
        self.assertEqual(result, [('HELLO', (0, 0), (0, 4))])

        # Test reverse horizontal search
        result = word_search.search_word('REW')
        self.assertEqual(result, [('REW', (3, 2), (3, 0))])

        # Test vertical search
        result = word_search.search_word('AYE')
        self.assertEqual(result, [('AYE', (1, 1), (3, 1))])

        # Test reverse vertical search
        result = word_search.search_word('RIO')
        self.assertEqual(result, [('RIO', (3, 2), (1, 2))])

        # Test forward diagonal search (top left to bottom right)
        result = word_search.search_word('HAIL')
        self.assertEqual(result, [('HAIL', (0, 0), (3, 3))])

        # Test reverse diagonal search (bottom right to top left)
        result = word_search.search_word('DLO')
        self.assertEqual(result, [('DLO', (3, 4), (1, 2))])

        # Test forward diagonal search (top right to bottom left)
        result = word_search.search_word('OOIE')
        self.assertEqual(result, [('OOIE', (0, 4), (3, 1))])

        # Test reverse diagonal search (bottom left to top right)
        result = word_search.search_word('WYOL')
        self.assertEqual(result, [('WYOL', (3, 0), (0, 3))])


if __name__ == '__main__':
    unittest.main()
