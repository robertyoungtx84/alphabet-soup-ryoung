class WordSearch:
    """
    Represents a word search puzzle grid.

    Attributes:
    - grid (list of lists): The grid representing the word search puzzle.

    Methods:
    - __init__(self, grid=None): Initializes a WordSearch instance with the provided grid.
    - search_word(self, word): Searches for a word in the puzzle grid and returns the positions where the word is found.
    """
    def __init__(self, grid=None):
        """
        Initializes a WordSearch instance with the provided grid.

        Parameters:
        - grid (list of lists, optional): The grid representing the word search puzzle. If not provided, an empty grid
          is created.

        Returns:
        - None
        """
        if grid is None:
            self.grid = []
        else:
            self.grid = grid

    def search_word(self, word):
        """
        Searches for a word in the puzzle grid and returns the positions where the word is found.

        Parameters:
        - word (str): The word to search for in the puzzle grid.

        Returns:
        - list of tuples: A list of tuples containing the word, starting position, and ending position for each
          occurrence of the word in the grid. Each tuple has the format (word, (start_row, start_col), (end_row, end_col)).
          If the word is not found in the grid, an empty list is returned.
        """
        result = []
        rows = len(self.grid)
        cols = len(self.grid[0])

        # Forward horizontal search
        for r in range(rows):
            for c in range(cols - len(word) + 1):
                if self.check_word(word, r, c, 0, 1):
                    result.append((word, (r, c), (r, c + len(word) - 1)))

        # Reverse horizontal search
        for r in range(rows):
            for c in range(cols - 1, len(word) - 2, -1):
                if self.check_word(word, r, c, 0, -1):
                    result.append((word, (r, c), (r, c - len(word) + 1)))

        # Forward vertical search
        for r in range(rows - len(word) + 1):
            for c in range(cols):
                if self.check_word(word, r, c, 1, 0):
                    result.append((word, (r, c), (r + len(word) - 1, c)))

        # Reverse vertical search
        for r in range(rows - 1, len(word) - 2, -1):
            for c in range(cols):
                if self.check_word(word, r, c, -1, 0):
                    result.append((word, (r, c), (r - len(word) + 1, c)))

        # Forward diagonal search (top-left to bottom-right)
        for r in range(rows - len(word) + 1):
            for c in range(cols - len(word) + 1):
                if self.check_word(word, r, c, 1, 1):
                    result.append((word, (r, c), (r + len(word) - 1, c + len(word) - 1)))

        # Forward diagonal search (top-right to bottom-left)
        for r in range(rows - len(word) + 1):
            for c in range(len(word) - 1, cols):
                if self.check_word(word, r, c, 1, -1):
                    result.append((word, (r, c), (r + len(word) - 1, c - len(word) + 1)))

        # Reverse diagonal search (bottom-left to top-right)
        for r in range(rows - 1, len(word) - 2, -1):
            for c in range(cols - len(word) + 1):
                if self.check_word(word, r, c, -1, 1):
                    result.append((word, (r, c), (r - len(word) + 1, c + len(word) - 1)))

        # Reverse diagonal search (bottom-right to top-left)
        for r in range(rows - 1, len(word) - 2, -1):
            for c in range(cols - 1, len(word) - 2, -1):
                if self.check_word(word, r, c, -1, -1):
                    result.append((word, (r, c), (r - len(word) + 1, c - len(word) + 1)))

        
        return result
    
    
    def check_word(self, word, start_row, start_col, delta_row, delta_col):
        """
        Checks if a word can be found in the grid starting from a given position and moving in a specified direction.

        Parameters:
        - word (str): The word to search for in the grid.
        - start_row (int): The starting row index in the grid.
        - start_col (int): The starting column index in the grid.
        - delta_row (int): The change in row index for each step in the search direction.
        - delta_col (int): The change in column index for each step in the search direction.

        Returns:
        - bool: True if the word is found in the grid starting from the given position and moving in the specified direction,
        False otherwise.
        """
        for i in range(len(word)):
            if (
                start_row + i * delta_row >= len(self.grid)
                or start_row + i * delta_row < 0
                or start_col + i * delta_col >= len(self.grid[start_row])
                or start_col + i * delta_col < 0
                or self.grid[start_row + i * delta_row][start_col + i * delta_col] != word[i]
            ):
                return False
        return True
