from sys import argv
from alphabet_soup4 import WordSearch
"""
Manual Test Script for the WordSearch Class

This script demonstrates the usage of the WordSearch class by reading a puzzle grid and a list of words from a file,
performing the word search, and displaying the results.

Usage:
- Run the script.
- Enter the filename as first argument.
- The script will read the puzzle grid and words from the file, perform the word search, and display the results.

"""

# Check if the filename is provided as a command-line argument
if len(argv) != 2:
    print("Error: Please provide the filename as a command-line argument.")
    exit(1)

# Extract the filename from the command-line argument
filename = argv[1]

# Read the contents of the file
try:
    with open(filename, 'r') as file:
        lines = file.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    exit(1)

# Extract the grid size
grid_size = lines[0].strip()

# Extract the grid
grid_lines = lines[1:1+int(grid_size.split('x')[0])]
grid = [line.strip().split() for line in grid_lines]

# Extract the words to search for
words = [word.strip() for word in lines[1+int(grid_size.split('x')[0]):]]

# Create an instance of WordSearch
my_word_search = WordSearch(grid)

# Call the search_word method for each word
for word in words:
    result = my_word_search.search_word(word)
    if not result:
        #Debug print
        #print(f"The word '{word}' was not found in the grid.")
        result = []
    else:
        #Debug print
        #print(f"The word '{word}' was found at the following positions:")
        for word, start, end in result:
            print(f"{word} {start[0]}:{start[1]} {end[0]}:{end[1]}")

# Debug: Display all the words at the end
#print("All words in the input file:")
#print(', '.join(words))
